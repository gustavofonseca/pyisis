��    9      �  O   �      �     �  +   	  9   5  -   o  &   �  (   �  ,   �        #   ;     _     z  7   �  .   �     �                   :  "   T     w  *   �  0   �  /   �  8   	     R	  !   p	      �	     �	  "   �	  '   �	     
  $   <
     a
  0   t
  !   �
     �
  &   �
  *     #   0  *   T          �     �  "   �  B   �  (   :  ;   c  F   �  (   �       *   "  ,   M  6   z  )   �  0   �  ,     0  9  #   j  &   �  0   �  ,   �  .     ,   B  ,   o  #   �  *   �     �       J   $  ?   o     �     �    �     �               :  '   O  9   w  8   �  8   �     #     A  !   `  "   �  $   �  +   �  &   �  #        A  <   Z  $   �     �  &   �  .   �  #   (  -   L     z  !   �  "   �  $   �  3   �  .   1  >   `  Q   �  "   �       >   '  =   f  7   �  +   �  6     5   ?     2                    0            &   #   9      
       -   "              4   ,   5                                                      3                $                8      !          1             )           *   7       .   +         (       %         	   6      '       /    %s (next mfn:%s, type:%s) in %s %s is not an attribute of the collection %s <h3>@pft must be called after record resource not %s</h3> Action range requires at least one parameter. Asked to undelete record flagged as %s Browse the the master file interactively Closing ISIS-NBP Interactive Python Console
 Collection %s with databases: %s Convert master file contents to XML Copy inputdb into outputdb Dumps the control record ERROR: config file %s with invalid encoding entry: %s.
 ERROR: config file %s with invalid entry: %s.
 Elapsed time: Empty %s file Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.
You'll have to run django-admin.py, passing it your settings module.
(If the file settings.py does indeed exist, it's causing an ImportError somehow.)
 Expecting <typename>,<path>.
 Expecting True or False.
 Failed to create collection %s: %s Failed to open %s: %s File %s does not exist or is inaccessible. Gateway ready to handle HTTP requests at port %s Gateway ready to handle SSH requests at port %s Illegal character '%s' at position %d in expression {%s} Interactive mode of operation Invalid boolean function named %s Invalid conditional literal {%s} Invalid field expression {%s} Invalid inconditional literal {%s} Invalid parameter for boolean function. Invalid repeatable literal {%s} List the contents of the master file No resource found! Not implemented xrf handling of invalid records. Opening file %s for collection %s Prints current version Profile the execution of --list option Specify the path to the configuration file Specify the path to the master file Specify the path to the output master file Start in server mode. Syntax error at '%s'. Type: %s Syntax error. Empty production. The available collections are: %s
 This codec is either not registered or it is spelled incorrectly.
 Tried to save record flagged as invalid. Type '<Ctrl-D>' or 'exit()' followed by '<enter>' to quit.
 Type 'collection' to see a dictionary with all available collections.
 Unexpected error while processing %s: %s Unexpected type %s Use -m or --inputdb to set input database. Use -o or --outputdb to set output database. Use the console to test and inspect the collections.

 Value is not recognised as a valid number Welcome to ISIS-NBP Cell %s Interactive Console
 Word %s is not recognised as a valid keyword Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-07-25 10:06+BRT
PO-Revision-Date: 2008-07-25 10:13-0300
Last-Translator: joao <EMAIL@ADDRESS>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit %s (próximo mfn:%s, tipo:%s) em %s %s não é um atributo da coleção %s <h3>@pft deve ser chamada após registro %s</h3> Ação faixa exige pelo menos um parâmetro. Perguntar para apagar registro marcado como %s Inspecionar o arquivo master interativamente Fechando Console Python Interativo ISIS-NBP
 Coleção %s com banco de dados: %s Converte conteúdo arquivo master para XML Copia inputdb em outputdb Imprime o registro de controle Erro: arquivo de configuração %s com entrada de encoding inválida: %s.
 Erro: arquivo de configuração %s com entrada %s inválida %s
 Tempo decorrido: Arquivo %s vazio Erro: Arquivo 'settings.py' não encontrado no diretório %r. Aparentemente você customizou alguns parâmetros.
É necessário executar django-admin.py, passando seu módulo de configuração.
(Se o arquivo settings.py existe, este está causando um erro de importação.)
 Experado <typename>,<path> 
 Esperado True ou False. 
 Falha criando coleção %s: %s Falha abrindo %s: %s Arquivo %s não existe ou inacessível. Gateway pronto para tratar requisições HTTP na porta %s Gateway pronto para tratar requisições SSH na porta %s Caracter ilegal '%s' na posição %d na expressão {%s}  Modo interativo de operação Função booleana %s inválida Condição literal inválida {%s} Campo de expressão inválida {%s} Literal incondicional inválida {%s} Parâmtro inválido para função booleana. Literal com repetição inválida {%s} Lista o conteúdo do arquivo master Recurso não encontrado! Não implementado manipulação de registros inválidos xrf  Abrindo arquivo %s para coleção %s Imprime versão atual Perfil de execução da opção --list Informe o caminho do arquivo de configuração Informe o caminho do arquivo master Informe o caminho do arquivo master de saída Iniciar em modo servidor Erro de sintaxe em '%s'. Tipo: %s Erro de sintaxe, produção vazia. As coleções disponíveis são: %s
 Este codec não foi registrado ou está incorreto.
 Tentou salvar registro marcado como inválido. Tecle '<Ctrl-D>' or 'exit()' seguido por '<enter>' para sair.
 Tecle 'collection' para ver um dicionário com todas as coleções disponíveis.
 Erro inexperado processando %s: %s Tipo %s inesperado Utilize -m ou --inputdb para marcar banco de dados de entrada. Utilize -o ou --outputdb para marcar banco de dados de saída Use o console para testar e inspecionar as coleções

 Valor não reconhecido como número válido Bem-vindo para Console Interativo Célula ISIS-NBP %s
 Palavra %s não foi reconhecida como uma plavra-chave 